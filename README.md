## Xiaomi Firmware Packages For Mi MIX 2 (chiron)

#### Downloads: [![DownloadSF](https://img.shields.io/badge/Download-SourceForge-orange.svg)](https://sourceforge.net/projects/yshalsager/files/Stable) [![DownloadAFH](https://img.shields.io/badge/Download-AndroidFileHost-brightgreen.svg)](https://www.androidfilehost.com/?w=files&flid=268046)

| ID | MIUI Name | Device Name | Codename |
| --- | --- | --- | --- |
| 334 | MIMIX2 | Xiaomi Mi MIX 2 | chiron |
| 334 | MIMIX2Global | Xiaomi Mi MIX 2 Global | chiron |

### XDA Support Thread For chiron:
[Go here](https://forum.xda-developers.com/mi-mix-2/development/firmware-xiaomi-mi-mix-2-t3741667)

### XDA Main Thread:
[Go here](https://forum.xda-developers.com/android/software-hacking/devices-yshalsager-t3741446)

#### by [Xiaomi Firmware Updater](https://github.com/XiaomiFirmwareUpdater)
#### Developer: [yshalsager](https://github.com/yshalsager)
